**继承的方式**

**1.原型链继承**
```js
function P(name: string){
  this.name = name
}

P.prototype.sayName = function(){
  console.log('P', this.name)
}

function C(name: string){
  this.name = name
}

C.prototype = new P('P')
C.prototype.constructor = C

C.prototype.sayName = function(){
  console.log('C', this.name)
}

const child = new C('C')
child.sayName() // 'C' C

```
>缺点:
>1. 子类型无法给超类型传递参数，在面向对象的继承中，我们总希望通过 var child = new Child('son', 'father'); 让子类去调用父类的构造器来完成继承。而不是通过像这样 new Parent('father') 去调用父类。
>2. 引用类型值的原型属性会被所有实例共享

**2.借用构造函数(经典继承)**
```js
function P(age: number){
  this.names = ['alex', 'jet']
  this.age = age
}

function C(age: number){
  P.call(this, age)
}

const child = new C()
child.names.push('mark')  
child.names         // alex jet mark

const child1 = new C()
child1.names        // alex jet

const child = new C('15')
child.age           // 15

const child = new C('18')
child.age           // 18

```
>优点:
>1. 避免了引用类型的属性被所有实例共享
>2. 可以在 Child 中向 Parent 传参

>缺点：方法都在构造函数中定义，因此函数复用就无从谈起了。而且，在超类型的原型中定义的方法，对子类型而言也是不可见的。

**3.组合继承**

```js
function P(name: string){
  this.name = name
  this.colors = ['yellow', 'blue']
}

P.prototype.getName = function(){
  console.log(this.name)
}

function C(name: string, age: number){
  P.call(this, name)
  this.age = age
}

C.prototype = new P()

const child = new C('jet', 18)
child.colors.push('black')  // yellow blue black
child.name                  // jet
child.age                   //  18

const child1 new C('jack', 20)
child1.colors               // yellow blue
child1.name                 // jack
child1.age                  // 20    
```

>优点:
>1. 融合原型链继承和构造函数的优点，是 JavaScript 中最常用的继承模式。
>2. 都会调用两次超类型构造函数

**4.原型式继承**

```js
function object(o: Object){ 
  function F(){}
  F.prototype = o
  return new F()
}

var person = { 
  name: "Nicholas", 
  friends: ["Shelby", "Court", "Van"] 
}
 
var anotherPerson = object(person)
anotherPerson.name = "Greg"
anotherPerson.friends.push("Rob")
 
var yetAnotherPerson = object(person)
yetAnotherPerson.name = "Linda"
yetAnotherPerson.friends.push("Barbie")
 
alert(person.friends)   //"Shelby,Court,Van,Rob,Barbie" 
```

>特点：包含引用类型值的属性始终都会共享相应的值，就像使用原型模式一样

**5.寄生式继承**

>思想： 寄生式继承的思路与寄生构造函数和工厂模式类似，即创建一个仅用于封装继承过程的函数，该函数在内部以某种方式来增强对象，最后再像真地是它做了所有工作一样返回对象
```js
function createAnother(original){ 
  var clone = object(original)   //通过调用函数创建一个新对象 
  clone.sayHi = function(){      //以某种方式来增强这个对象 
    alert("hi") 
  } 
  return clone                   //返回这个对象 
}
```

>缺点: 使用寄生式继承来为对象添加函数，会由于不能做到函数复用而降低效率

**6.寄生组合式继承**

```js
function inheritPrototype(subType, superType){ 
  var prototype = object.create(superType.prototype);     //创建对象 
  prototype.constructor = subType;                        //增强对象 
  subType.prototype = prototype;                          //指定对象 
}
```
>优点: 只调用了一次 SuperType 构造函数，并且因此避免了在 SubType. 
prototype 上面创建不必要的、多余的属性。与此同时，原型链还能保持不变；因此，还能够正常使用instanceof 和 isPrototypeOf()