**JS事件机制**

三个阶段：JavaScript 事件触发有三个阶段，即捕获阶段，即目标阶段，即冒泡阶段

![image](./img/1641b55b628f6727.webp)

```
<div id="s1">
  s1
  <div id="s2">s2</div>
</div>

s1.addEventListener("click",function(e){ 
  console.log("s1 冒泡事件")
}, false)
s2.addEventListener("click",function(e){ 
  console.log("s2 冒泡事件")
}, false)
s1.addEventListener("click",function(e){ 
  console.log("s1 捕获事件")
}, true)
s2.addEventListener("click",function(e){
  console.log("s2 捕获事件")
}, true)

点击s2，click事件从document->html->body->s1->s2(捕获前进)这里在s1上发现了捕获注册事件，则输出"s1 捕获事件"到达s2，已经到达目的节点。s2上注册了冒泡和捕获事件，先注册的冒泡后注册的捕获，则先执行冒泡，输出"s2 冒泡事件", 再在s2上执行后注册的事件，即捕获事件，输出"s2 捕获事件"。

s1 捕获事件
s2 冒泡事件
s2 捕获事件
s1 冒泡事件

```