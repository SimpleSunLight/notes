```js
// call
Function.prototype.mycall = function (ctx) {
  ctx = ctx || window
  ctx.fn = this
  const args = []
  for (let i = 1; i < arguments.length; i++) {
    args.push(arguments[i])
  }
  const result = ctx.fn(...args)
  delete ctx.fn
  return result
}

const obj = {
  name: "jack",
}

function show(name) {
  console.log("show", this.name)
}

show.mycall(obj)
```

```js
// apply
Function.prototype.myapply = function (ctx, arr) {
  ctx = ctx || window
  ctx.fn = this
  const result = null
  const args = []
  if (!arr.length) {
    result = ctx.fn()
  }else {
    if (!(arr instanceof Array)) {
      throw new Error("arr must be array")
    }
    for (let i = 1; i < arguments.length; i++) {
      args.push(arguments[i])
    }
    result = ctx.fn(...args)
    delete ctx.fn
  }
  return result
}
```

```js
// bind
Function.prototype.mybind = function (ctx) {
  ctx = ctx || window
  const self = this
  const args = Array.prototype.slice.call(arguments, 1)
  if(typeof this !== 'funciton'){
    throw new Error('this must be a callback')
  }
  function temp() {}
  const fn = function(){
    cosnt fnArgs = Array.prototype.slice.call(arguments, 0)
    return self.apply(this instanceof fn ? this: ctx, fnArgs.concat(...args))
  }
  temp.prototype = this.prototype
  fn.prototype = new temp() // 原型继承
  return fn
}

var obj1 = {
  value: "obj",
}
Parent.prototype.value = "parent"
function Parent() {}
Child.prototype = new Parent()
function Child() {}
function show(name) {
  console.log(this.value, name)
}

var oS = Child.es3bind(obj)
var fn = new oS()
console.log(fn, fn.value)
```
