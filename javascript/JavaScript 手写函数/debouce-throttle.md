#### 防抖

>防抖：任务频繁触发的情况下，只有任务触发的间隔超过指定间隔的时候，任务才会执行。
举例：
  1. 监听拖拽滚动条，然后频繁向下滚动信息，会变得很慢，很迟钝。
  2. 点击提交表单后，用户在结果还没出来的时候重复触发。

简单来说：某件事你并不想它太过频繁触发，那么设置一个定时器，每次进来的时候都清除原本的定时器，然后重新开始计时。

```js
const debouce = (func, interval = 3000) => {
  let timer = null
  return function () {
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      func.apply(this, arguments)
    }, interval)
  }
}
```

#### 节流

>节流：指定时间间隔内只会执行一次任务。举例：
  1. 你不想频繁为你女票买单，于是约好每月 1 号清空购物车
  2. 防止过快拖动滚动条，导致 JS 跟不上拖动频率，通过节流限制每秒触发监听的次数（固定时间固定频率）

简单来说：年轻人要保持一日三餐，规律作息，那就通过节流来限制。

```js
const throttle = (func, interval = 3000) => {
  let lastTime = 0
  return function () {
    let nowTime = +new Date()
    if (nowTime - lastTime > interval) {
      func.apply(this, arguments)
    }
  }
}
```
