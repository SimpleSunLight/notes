# React坑点 #

1、JSX做表达式判断时候，需要强转为boolean类型，如
```
render() {
  const b = 0;
  return <div>
    {
      !!b && <div>这是一段文本</div>
    }
  </div>
}

```
如果不使用 !!b 进行强转数据类型，会在页面里面输出 0


2、尽量不要在 componentWillReviceProps 里使用 setState，如果一定要使用，那么需要判断结束条件，
不然会出现无限重渲染，导致页面崩溃。(实际不是componentWillReviceProps会无限重渲染，而是componentDidUpdate)


3、给组件添加ref时候，尽量不要使用匿名函数，因为当组件更新的时候，匿名函数会被当做新的prop处理，
让ref属性接受到新函数的时候，react内部会先清空ref，也就是会以null为回调参数先执行一次ref这个props，然后在以该组件的实例执行一次ref，
所以用匿名函数做ref的时候，有的时候去ref赋值后的属性会取到null。

4、遍历子节点的时候，不要用 index 作为组件的 key 进行传入。

Q2: 我现在有一个button，要用react在上面绑定点击事件，要怎么做？

```
class Demo {
  render() {
    return <button onClick={(e) => {
      alert('我点击了按钮')
    }}>
      按钮
    </button>
  }
}
```

Q3: 接上一个问题，你觉得你这样设置点击事件会有什么问题吗？
由于onClick使用的是匿名函数，所有每次重渲染的时候，会把该onClick当做一个新的prop来处理，会将内部缓存的onClick事件进行重新赋值，所以相对直接使用函数来说，可能有一点的性能下降
```
class Demo {

  onClick = (e) => {
    alert('我点击了按钮')
  }

  render() {
    return <button onClick={this.onClick}>
      按钮
    </button>
  }
}
```
当然你在内部声明的不是箭头函数，然后你可能需要在设置onClick的时候使用bind绑定上下文，这样的效果和先前的使用匿名函数差不多，因为bind会返回新的函数，也会被react认为是一个新的prop。

```
class Demo {

  onClick = (e) => {
    alert('我点击了按钮')
  }

  render() {
    return <button onClick={this.onClick}>
      按钮
    </button>
  }
}

```

```
class Demo {

  onClick(e) {
    alert('我点击了按钮')
  }

  render() {
    return <button onClick={this.onClick.bind(this)}>
      按钮
    </button>
  }
}

```

```
class Demo {

  onClick(e) {
    alert('我点击了按钮')
  }

  render() {
    return <button onClick={() => this.onClick()}>
      按钮
    </button>
  }
}

```
在 render 方法中使用箭头函数也会在每次组件渲染时创建一个新的函数，这会破坏基于恒等比较的性能优化,当然你在内部声明的不是箭头函数，然后你可能需要在设置onClick的时候使用bind绑定上下文，这样的效果和先前的使用匿名函数差不多，因为bind会返回新的函数，且bind实现很复杂很消耗性能，也会被react认为是一个新的prop。