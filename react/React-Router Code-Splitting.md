import React from 'react'

export default asyncComponent = (importComponent) => {
	return class AsyncComponent extends React.Component{
		constructor(props){
			super(props)
			this.state = {
				component: null
			}
		}

		async componentDidMount() {
			const { default: component } = await importComponent()
			this.setState({
				component: component
			})
		}

    render() {
      const { C } = this.state
      return C ? <C {...this.props} /> : null
		}
	}
}

//Q:为什么是以 () => import('./LoginContainer') 这样的箭头函数为参数
//A:和 Webpack 的进行代码分片的机制有关,可以让我们控制生成多少个 .chunk.js 这样的分片文件 ，chromec下面Network可看到多个chunk.js