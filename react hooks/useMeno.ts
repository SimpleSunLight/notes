import * as React from 'react'
// useCallback(fn, inputs) is equivalent to useMemo(() => fn, inputs)

const App = () => {
  const memoizedHandleClick = React.useMemo(() => () => {
    console.log('Click happened')
  }, []) // 空数组代表无论什么情况下该函数都不会发生改变
  return <SomeComponent onClick={memoizedHandleClick}>Click Me</SomeComponent>;
}

//useCallback 不会执行第一个参数函数，而是将它返回给你，
//useCallback 常用记忆事件函数，生成记忆后的事件函数并传递给子组件使
//useMemo 更适合经过函数计算得到一个确定的值
//useMemo 会执行第一个函数并且将函数执行结果返回给你

const Parent = ({ a, b }) => {
  // Only re-rendered if `a` changes:
  const child1 = React.useMemo(() => <Child1 a={a} />, [a]);
  // Only re-rendered if `b` changes:
  const child2 = React.useMemo(() => <Child2 b={b} />, [b]);
  return (
    <>
      {child1}
      {child2}
    </>
  )
}