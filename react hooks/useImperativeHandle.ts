import * as React from 'react'

const = FancyInput(props, ref) => {

  const inputRef = React.useRef()

  React.useImperativeHandle(ref, () => ({
    focus: () => {
      inputRef.current.focus()
    }
  }))
  return <input ref={inputRef} ...props />
}

FancyInput = React.forwardRef(FancyInput)

const App = () => {

	const FancyInputRef = React.useRef(null)

  React.useEffect(() => {
    FancyInputRef.current.focus()
  }, [])

	retrun <div>
		<FancyInput ref={FancyInputRef} />
	</div>	
}

//useImperativeHandle 用于让父组件获取子组件内的Dom