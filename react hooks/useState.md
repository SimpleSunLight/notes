import * as React from 'react'

const App = () => {
	const [ obj, setObject] = React.useState({
		count: 0,
		name: 'alex'
	})
	// const [count, setCount] = useState(0)
	// const [fruit, setFruit] = useState('banana')
  // const [todos, setTodos] = useState([{ text: 'Learn Hooks' }])

	return <div>
		<p>current value {obj.count}</p>
		// setCount 覆盖式更新 setState调合Object.assign()
		<button onClick={() => setCount({ ...obj, count: count + 1})}>+</button>
		<button onClick={() => setCount({ ...obj, count: count + 1})}>-</button>
	</div>
}