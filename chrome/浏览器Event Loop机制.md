## 浏览器 Event Loop 机制

![../img/1641b55b628f6727.png](../img/1641b55b628f6727.png)

![../img/43201890-6139de56.png](../img/43201890-6139de56.png)

**流程**

**执行时 先微任务然后再是宏任务**

首先执行一个 task，如果整个第一轮 event loop，那么整体的 script 就是一个 task，同步执行的代码会直接放进 call stack（调用栈）中，诸如 setTimeout、fetch、ajax 或者事件的回调函数会由 Web APIs 进行管理，然后 call stack 继续执行栈顶的函数。当网络请求获取到了响应或者 timer 的时间到了，Web APIs 就会将对应的回调函数推入对应的 task queues 中。event loop 不断执行，一旦 event loop 中的 current task 为 null，它就回去扫 task queues 有没有 task，然后按照一定规则拿出 task queues 中一个最早入队的回调函数（比如上面提到的以 75% 的几率优先执行鼠标键盘的回调函数所在的队列，但是具体规则我还没找到），取出的回调函数放入上下文执行栈就开始同步执行了，执行完之后检查 event loop 中的 microtask queue 中的 microtask，按照规则将它们全部同步执行掉，最后完成 UI 的重渲染，然后再执行下一轮的 event loop...

>**JavaScript 的运行机制:**  
>1. 所有同步任务都在主线程上执行，形成一个执行栈（execution context stack）。
>2. 主线程之外，还存在"任务队列"(task queue)。只要异步任务有了运行结果，就在"任务队列"之中放置一个事件。
>3. 一旦"执行栈"中的所有同步任务执行完毕，系统就会读取"任务队列"，看看里面有哪些事件。那些对应的异步任务，于是结束等待状态，进入执行栈，开始执行。
>4. 主线程不断重复上面的第三步

概括即是: 调用栈中的同步任务都执行完毕，栈内被清空了，就代表主线程空闲了，这个时候就会去任务队列中按照顺序读取一个任务放入到栈中执行。每次栈内被清空，都会去读取任务队列有没有任务，有就读取执行，一直循环读取-执行的操作

>**JavaScript 中有两种异步任务:**
>1. 宏任务(macrotask): script（整体代码）, setTimeout, setInterval, setImmediate, I/O, UI rendering ==> webApis
>2. 微任务(microtask): process.nextTick（Nodejs）, Promise.prototype.then(), Object.observe, MutationObserver;

**setTimeout 的不准确性**

了解了上面 Web APIs，我们知道浏览器中有一个 Timers 的 Web API 用来管理 setTimeout 和 setInterval 等计时器，在同步执行了 setTimeout 后，浏览器并没有把你的回调函数挂在事件循环队列中。 它所做的是设定一个定时器。 当定时器到时后， 浏览器会把你的回调函数放在事件循环中， 这样， 在未来某个时刻的 tick 会摘下并执行这个回调。

但是如果定时器的任务队列中已经被添加了其他的任务，后面的回调就要等待。

```js
let t1, t2

t1 = new Date().getTime();

// 1
setTimeout(()=>{
	let i = 0;
	while (i < 50000000) {i++}
	console.log('block finished')
}, 300)

// 2
setTimeout(()=>{
	t2 = new Date().getTime();
	console.log(t2 - t1)
}, 300)

=> 372

console.log('begin');
setTimeout(() => {
  console.log('setTimeout 1');
  Promise.resolve()
    .then(() => {
      console.log('promise 1');
      setTimeout(() => {
        console.log('setTimeout2');
      });
    })
    .then(() => {
      console.log('promise 2');
    });
  new Promise(resolve => {
    console.log('a');
    resolve();
  }).then(() => {
    console.log('b');
  });
}, 0);
console.log('end');

// begin
// end
// setTimeout1
// a
// promise1
// b
// promise2
// setTimeout2

```

这个例子中，打印出来的时间戳就不会等于 300，虽然两个 setTimeout 的函数都会在时间到了时被 Web API 排入任务队列，然后 event loop 取出第一个 setTimeout 的回调开始执行，但是这个回调函数会同步阻塞一段时间，导致只有它执行完毕 event loop 才能执行第二个 setTimeout 的回调函数。
