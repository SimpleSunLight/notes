//! 1.给定一个字符串，找出其中无重复字符的最长子字符串长度        
//! 字节跳动前端面试题

const findNoRepeatLongStr = (str) => {
  let longStr = ''
  let maxLength = 0
  for(let i = 0; i < str.length; i++){
    const char = str.charAt(i)
    const index = longStr.indexOf(char)
    if(index === -1){
      longStr += char
      maxLength = longStr.length > maxLength ? longStr.length : maxLength
    }else{
      longStr = longStr.substr(index + 1) + char
    }
  }
  return {
    longStr,
    maxLength
  }
}

// console.log('zz', findNoRepeatLongStr('bbbbb'))                     // b   1
// console.log('zz', findNoRepeatLongStr('pwwkew'))                    // kew 3
// console.log('zz', findNoRepeatLongStr('abbbcbd'))                   // cbd 3
// console.log('zz', findNoRepeatLongStr('cfcfcfcfcf'))                // cf  2


//! 2.实现超出整数存储范围的两个大正整数相加
//! 腾讯前端面试题

//(-2^53---2^53)
let a = '11111111111111111111'
let b = '22222222222222222'

const maxNumAdd = (x, y) => {
  for(let i = 0; i < Math.max(a.length, b.length); i++){
    if(a.length > b.length){
      b = '0' + b
    }
    if(a.length < b.length){
      a = '0' + a
    }
  }
  const arrA = a.split('').reverse()
  const arrB = b.split('').reverse()
  const arr = Array.from(Array(Math.max(a.length, b.length))).map(t => 0)
  for(let i = 0; i < Math.max(a.length, b.length); i++){
    const temp = parseInt(arrA[i]) + parseInt(arrB[i])
    if(temp > 9){
      arr[i] = temp - 10
      arr[i+1] = arr[i+1] + 1
    }else{
      arr[i] = temp
    }
  }
  return arr.reverse().join('').toString()
}

// console.log('a + b', maxNumAdd(a, b))    // 11133333333333333333

//! 3.判断字符串是否是回文字符串

const isHuiWen = (str) => {
  if(!str.length) return false
  return str.split('').reverse().join('').toString() === str
}

//console.log(isHuiWen('aba'))    //true


//! 数组求和
const arr = [1,3,5,6,1,8,9,5,8,3,4,5]

const sum = arr.reduce((pre, cur) => {
	return pre + cur
}, 0)

// console.log(sum)                      // 58

//! 求数组最大值
const max = arr.reduce((pre, cur) => {
	return Math.max(pre, cur)
},0)

// console.log(max)                      // 9


//! 数组去重
const unique = arr.reduce((pre, cur) => {
	pre.indexOf(cur) === -1 && pre.push(cur)
	return pre
}, [])

// console.log(unique)                  // [1,3,5,6,8,9,4]


//! 斐波那契数列

const feibo = max => {
	let [a,b,i]= [0,1,1]
	while(i++<=max) {
		[a,b] = [b,a + b]
	}
	return a  
}

// console.log(feibo(10))                 // 55