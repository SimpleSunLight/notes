#### ***Topic1.实现一个累加函数的功能比***
  >如sum(1,2,3)(2).valueOf() 8  
  >函数柯里化

```js
const sum = (...num) => {
  const cur = (...count) => {
    return sum.apply(this, num.concat(count))
  }
  cur.valueOf = () => {
    return num.reduce((pre, cur) => pre + cur)
  }
  return cur
}
```
#### ***Topic2.1、1、2、3、5、8...计算第n个数的值（斐波那契数列）***

```js
const nNum = (n) => {
  if (n == 1 || n == 2) {
    return 1
  }
  return nNum(n - 1) + nNum(n - 2)
}
```
#### ***Topic3.1、4、5, n块组合个数最少***

```js
const minNum = (n) => {
  if (n % 5 === 0) {
    return n / 5
  } else if (n % 5 === 1) {
    return parseInt(n / 5) + 1
  } else if (n % 5 === 2) {
    return parseInt(n / 5) - 2 + 12 / 4
  } else if (n % 5 === 3) {
    return parseInt(n / 5) - 1 + 8 / 4
  } else if (n % 5 === 4) {
    return parseInt(n / 5) + 1
  }
}
```

#### ***Topic4.实现一个批量请求函数 multiRequest(urls, maxNum)，要求如下***
>要求最大并发数 maxNum  
>每当有一个请求返回，就留下一个空位，可以增加新的请求 
>所有请求完成后，结果按照 urls 里面的顺序依次打出

```js
const multiRequest = (urls = [], maxNum) => {
  const len = urls.length
  const result = new Array(len).fill(false)
  let count = 0

  return new Promise((resolve, reject) => {
    while (count < maxNum) {
      next()
    }
    function next() {
      let currentNum = count++
      if (currentNum >= len) {
        !result.includes(false) && resolve(result)
        return
      }
      fetch(urls[currentNum]).then(data => {
        result[currentNum] = data
        if (currentNum < len) {
          next()
        }
      }).catch(err => {
        result[currentNum] = err
        if (currentNum < len) {
          next()
        }
      })
    }
  })
}
```

#### ***Topic5.每次间隔3s输出Hello World***

```js
async function wait(seconds) {
  return new Promise((res) => {
    setTimeout(res, seconds);
  });
}

function repeat(func, times, s) {
  return async function (...args) {
    for (let i = 0; i < times; i++) {
      func.apply(null, args);
      await wait(s);
    }
  };
}

let log = console.log
let repeatFunc = repeat(log, 4, 3000)
// repeatFunc('hellworld');
```