// keyof
interface Point{
	x: number
	y: number
}
// type keys = "x" | "y"
type keys = keyof Point

// 获取一个属性的值
const data = {
	a: 'hello',
	b: 'world'
}

function get<T extends object, K extends keyof T>(o: T, name: K):T[K]{
	return o[name] 
}

// Required => 必需 & Partial => 可选 & Pick => 挑选 & Readonly => 只读  
type Readonly<T> = {
    readonly [P in keyof T]: T[P]
}

type Partial<T> = {
	[P in keyof T]?: T[P]
}

type Required<T> = {
	[P in keyof T]-?: T[P]
}

type Pick<T, K extends keyof T> = {
	[P in K] : T[P]
}

// Condition Type
T extends U ? X: Y

type isTrue<T> = T extends true ? true : false

type t = isTrue<number>  // false


// never => void & Exclude => 排除 & Omit => 删除指定的属性,保留剩下的属性

type Exclude<T, U> = T extends U ? never: T

type A = Exclude<'x'|'a', 'x'|'y'|'z'> // 'a'

type Omit<T, K extends keyof T> = Pick<T, Exclude<K extends keyof T>>

type OmitPoint = Omit<Point, 'x'> // { y: number }

// interface & type 
// type 可以声明基本类型别名，联合类型，元组等类型, type语句中还可以使用 typeof 获取实例的 类型进行赋值
// interface 能够声明合并

// Record => Copy一份 & Dictionary => 字典

type Record<K extends keyof any, T> = {
	[P in K]: T
}

type Dictionary<T>{
	[index: string]: T
}

type NumericDictionary<T>{
	[index:number]: T
}

const data:NumericDictionary<number> = {
	a: 1,
	b: 2
}

// enum 维护常量

const enum TODO_STATUS{
	TODO = 'TODO'
	DONE = 'DONE'
	DOING: 'DOING'
}

// 命名空间的运用

declare namespace D3{

	export interface Selectors{
		select: {
			(selector: string): Selections
			(element: EventTarget): Selections
		}
	}

	export interface Event{
		x: number
		y: number
	}

	export interface Base extends Selectors{
		event: Event
	}

}

declare const d3: D3.Base