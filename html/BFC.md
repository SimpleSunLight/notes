## 什么是 BFC

### BFC 是什么

BFC 英文解释： block formatting context。中文意思：块级格式化上下文；
formatting context 意思是：页面中一个渲染区域，有自己的一套渲染规则，决定其子元素如何定位，以及和其他兄弟元素的关系和作用。
BEC 有如下特性

> 内部的 box 会在垂直方向，从顶部开始一个接一个地放置；
> box 垂直方向的距离由 margin 决定。属于同一个 BFC 的两个相邻的 box 的 margin 会发生折叠；
> 每一个元素的 margin box 的左遍，与包含块 border box 的左边相接触。即使浮动元素也是如此；
> BFC 区域不会与 float box 叠加；
> 计算 BFC 的高度时，浮动子元素也参与计算；
> 文字层不会被浮动层覆盖，环绕于周围

### 产生 BFC 作用的 css 属性

> float 除了 none 以外的值；
> overflow 除了 visible 以外的值（hidden，auto，scroll ）；
> display (table-cell，table-caption，inline-block, flex, inline-flex)；
> position 值为（absolute，fixed） 这些属性值得元素都会自动创建 BFC；

### BFC 作用

BFC 最大的一个作用就是：在页面上有一个独立隔离容器，容器内的元素 和 容器 外的元素布局不会相互影响。

解决上外边距重叠；重叠的两个 box 都开启 bfc;
解决浮动引起高度塌陷；容器盒子开启 bfc;
解决文字环绕图片；左边图片 div，右边文本容器 p，将 p 容器开启 bfc；
