// 1.Object.defineProperty

// <input id='input' />
// <span id="span"></span>
const obj = {}
const input = document.getElementById('input')
const span = document.getElementById('span')

Object.defineProperty(obj, 'text', {
  get: function(){
  },
  set: function(value){
    input.value = value
    span.innerHTML = value
  }
})

input.addEventListener('keyup', function(e){
  obj.text = e.target.value
})

// 缺点1： 无法监听数组的变化 
// => 而Vue的文档提到了Vue是可以检测到数组变化的，但是只有以下八种方法,vm.items[indexOfItem] = newValue这种是无法检测 ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse']
const aryMethods = ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse']
const arrayAugmentations = []

aryMethods.forEach(method => {
  // 这里是原生Array的原型方法
  let original = Array.prototype[method]

  // 将push, pop等封装好的方法定义在对象arrayAugmentations的属性上
  // 注意：是属性而非原型属性
  arrayAugmentations[method] = function () {
    // 调用对应的原生方法并返回结果
    return original.apply(this, arguments)
  }
})

let list = ['a', 'b', 'c']
// 将我们要监听的数组的原型指针指向上面定义的空数组对象 别忘了这个空数组的属性上定义了我们封装好的push等方法
list.__proto__ = arrayAugmentations
list.push('d') // 我被改变啦！ 4

// 这里的list2没有被重新定义原型指针，所以就正常输出
let list2 = ['a', 'b', 'c']
list2.push('d') // 4

// 缺点2： 只能劫持对象的属性,因此我们需要对每个对象的每个属性进行遍历，如果属性值也是对象那么需要深度遍历,显然能劫持一个完整的对象是更好的选择 
// => Object.keys(value).forEach(key => this.convert(key, value[key]))



// 1.Object.Proxy

// <input id='input' />
// <span id="span"></span>

const obj = {}
const input = document.getElementById('input')
const p = document.getElementById('p')

const newObj = new Proxy(obj, {
  get: function(target, key, receiver) {
    return Reflect.get(target, key, receiver)
  },
  set: function(target, key, value, receiver) {
    if (key === 'text') {
      input.value = value
      p.innerHTML = value
    }
    return Reflect.set(target, key, value, receiver)
  }
})

input.addEventListener('keyup', function(e) {
  newObj.text = e.target.value
})

// 优点： Proxy可以直接监听对象而非属性
// 优点： Proxy可以直接监听数组的变化
// 优点： Proxy有多达13种拦截方法,不限于apply、ownKeys、deleteProperty、has等等是Object.defineProperty不具备的。
// 优点： Proxy返回的是一个新对象,我们可以只操作新的对象达到目的,而Object.defineProperty只能遍历对象属性直接修改。
// 优点： Proxy作为新标准将受到浏览器厂商重点持续的性能优化，也就是传说中的新标准的性能红利。
// 优点： 当然,Proxy的劣势就是兼容性问题,而且无法用polyfill磨平,因此Vue的作者才声明需要等到下个大版本(3.0)才能用Proxy重写。